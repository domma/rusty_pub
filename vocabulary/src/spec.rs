use std::collections::HashMap;

use scraper::{Html, ElementRef, Selector};
use reqwest::blocking;
use serde::Serialize;

#[derive(Debug, Serialize, Clone)]
pub struct Row {
    pub name: String,
    pub props: HashMap<String, String>
}


pub struct Spec(Html);

impl Spec {
    pub fn from_url(url: &str) -> Spec {
        Spec(Html::parse_document(
            &blocking::get(url).unwrap().text().unwrap()
        ))
    }

    fn get_section<'a>(self: &'a Spec, id: &str) -> Option<ElementRef<'a>> {
        let selector = Selector::parse("section").unwrap();

        for node in self.0.select(&selector) {
            if let Some(section_id) = node.value().attr("id") {
                if section_id == id {
                    return Some(node);
                }
            }
        }
        None
    }

    pub fn get_rows(self: &Spec, id: &str) -> Vec<Row> {

        //  Find the requested section in the html document
        let section = self.get_section(id).expect(
            &format!("Section '{}' could not be found.", id)
        );

        //  precompiled regex to normalize whitespace
        let ws = regex::Regex::new(r"\s+").unwrap();

        let tbody = Selector::parse("tbody").unwrap();
        section.select(&tbody).map(
            |item| {
                //  get all tds for the current row
                let mut tds = item.select(&Selector::parse("td").unwrap()).map(|td| text(&td)).collect::<Vec<_>>();

                //  remove the textual description on the right
                tds.remove(3);

                //  get the name of the row from the first column
                //  and remove it afterwards
                let name = tds[0].clone();
                tds.remove(0);

                //  the remining tds are key/value pairs which are consumed
                //  in pairs and converted into a hashmap.  
                let mut it = tds.iter();
                let mut props = HashMap::<String,String>::new();

                while let Some(key) = it.next() {
                    let val = it.next().expect("Could not get a value for the current key. Iterator seems to be broken, i.e. not consist of equal number of elements.");
                    props.insert(
                        key.to_string(),
                        ws.replace_all(val, " ").to_string()
                    );
                }

                Row {name, props}
            }
        ).collect::<Vec<_>>()
    }
}


fn text(node: &ElementRef) -> String {
    node.text().map(|t|t.to_string()).collect::<Vec<_>>().join("").trim().to_string()
}
