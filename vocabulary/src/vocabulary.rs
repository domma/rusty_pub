use serde::Serialize;
use std::collections::HashSet;
use regex;

use crate::spec::{Spec, Row};
//  Type

#[derive(Debug, Serialize)]
pub struct Type {
    pub name: String,
    pub uri: String,
    pub note: String,
    pub inherited: Inherited,
}

impl Type {
    fn from_row(row: &Row) -> Type {
        Type{
            name: row.name.clone(),
            uri: row.props["URI:"].clone(),
            inherited: Inherited::parse(&row.props["Properties:"]),
            note: row.props["Notes:"].clone(),
        }
    }

    fn parse_spec(spec: &Spec) -> Vec<Type> {
        spec.get_rows("types").iter()
            .chain(spec.get_rows("activity-types").iter())
            .chain(spec.get_rows("actor-types").iter())
            .chain(spec.get_rows("object-types").iter())
        .map(|row|Type::from_row(&row))
        .collect::<Vec<Type>>()
    }
}




//  Property

#[derive(Debug, Serialize)]
pub struct Property {
    name: String,
    uri: Option<String>,
    notes: Option<String>,
    range: Vec<String>
}

impl Property {
    fn from_row(row: &Row) -> Property {
        let props = &row.props;

        let uri = if let Some(u) = props.get("URI:") {
            Some(u.clone())
        } else {
            None
        };

        let range = if let Some(r) = props.get("Range:") {
            r.split("|").map(|x|x.trim().to_string()).filter(|x|!x.is_empty()).collect::<Vec<_>>()
        } else {
            vec![]
        };

        let notes = if let Some(n) = props.get("Notes:") {
            Some(n.trim().to_string())
        } else {
            None
        };

        Property {
            name: row.name.clone(),
            uri,
            notes,
            range
        }
    }

    fn parse_spec(spec: &Spec) -> Vec<Property> {
        spec.get_rows("properties")
            .iter()
            .map(|row|Property::from_row(&row))
            .collect()
    }
}



#[derive(Debug, Serialize)]
pub struct Vocabulary {
    pub types: Vec<Type>,
    pub properties: Vec<Property>
}

impl Vocabulary {
    pub fn from_spec(spec: &Spec) -> Vocabulary {
        Vocabulary {
            types: Type::parse_spec(&spec),
            properties: Property::parse_spec(&spec)
        }
    }

    pub fn type_by_name(self: &Self, name: &str) -> &Type {

        for t in self.types.iter() {
            if t.name == name {
                return &t;
            }
        }

        panic!("Type not found: {}", name);
    }

    pub fn get_property_names(self: &Self, t: &Type) -> Vec<String> {

        let mut result = HashSet::new();

        for p in t.inherited.properties.iter() {
            result.insert(p.clone());
        }

        for b in t.inherited.bases.iter() {
            let t = self.type_by_name(b);

            for p in self.get_property_names(t) {
                result.insert(p.clone());
            }
        }

        result.into_iter().collect()
    }
}





#[derive(Debug, Serialize)]
pub struct Inherited {
    properties: Vec<String>,
    bases: Vec<String>,
    except: Vec<String>
}

impl Inherited {
    fn parse(text: &str) -> Inherited {
        let re = regex::Regex::new(r"^(.*?)(?:Inherits all (?:.*?m )(\S*?(?: and \S*?)?)(?: except (.*))?)?.$").unwrap();
        let res = re.captures(&text).unwrap();

        let properties = if let Some(p) = res.get(1) {
            p.as_str().split("|").map(|x|x.trim().to_string()).filter(|x|!x.is_empty()).collect::<Vec<_>>()
        } else {
            Default::default()
        };

        let bases = if let Some(inherit) = res.get(2) {
            inherit.as_str().split(" and ").map(|i|i.trim().to_string()).filter(|x|!x.is_empty()).collect::<Vec<_>>()
        } else {
            Default::default()
        };

        let except = if let Some(except) = res.get(3) {
            vec![except.as_str().to_string()]
        } else {
            Default::default()
        };

        Inherited{properties, bases, except}
    }
}
