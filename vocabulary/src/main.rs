//use codegen::Scope;
use std::fs::File;
use codegen::Scope;
use std::io::Write;

mod spec;
mod vocabulary;


use crate::spec::Spec;
use crate::vocabulary::Vocabulary;




fn main() {
    let vocabulary = Vocabulary::from_spec(
        &Spec::from_url("https://www.w3.org/TR/activitystreams-vocabulary/")
    );

    let mut scope = Scope::new();

    scope.raw("// Generate code. Do not change. Will be overwritten automatically!");


    for t in vocabulary.types.iter() {
        let s = scope.new_struct(&t.name)
            .derive("Debug")
            .field("two", "String");

        for p in vocabulary.get_property_names(t).iter() {
            s.new_field(p, "usize").annotation("//serde");
        }
    }

    let mut out = File::create("../rusty-hub/src/activitypub/vocabulary.rs").expect("failed");
    write!(out, "{}", scope.to_string()).unwrap();
}
