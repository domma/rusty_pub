// Generate code. Do not change. Will be overwritten automatically!

#[derive(Debug)]
struct Object {
    two: String,
    //serde
    updated: usize,
    //serde
    duratio: usize,
    //serde
    inReplyTo: usize,
    //serde
    bcc: usize,
    //serde
    startTime: usize,
    //serde
    replies: usize,
    //serde
    name: usize,
    //serde
    context: usize,
    //serde
    published: usize,
    //serde
    url: usize,
    //serde
    bto: usize,
    //serde
    mediaType: usize,
    //serde
    image: usize,
    //serde
    attributedTo: usize,
    //serde
    content: usize,
    //serde
    generator: usize,
    //serde
    attachment: usize,
    //serde
    icon: usize,
    //serde
    location: usize,
    //serde
    preview: usize,
    //serde
    endTime: usize,
    //serde
    summary: usize,
    //serde
    tag: usize,
    //serde
    to: usize,
    //serde
    cc: usize,
    //serde
    audience: usize,
}

#[derive(Debug)]
struct Link {
    two: String,
    //serde
    hreflang: usize,
    //serde
    height: usize,
    //serde
    width: usize,
    //serde
    href: usize,
    //serde
    rel: usize,
    //serde
    mediaType: usize,
    //serde
    previe: usize,
    //serde
    name: usize,
}

#[derive(Debug)]
struct Activity {
    two: String,
    //serde
    replies: usize,
    //serde
    icon: usize,
    //serde
    to: usize,
    //serde
    name: usize,
    //serde
    inReplyTo: usize,
    //serde
    cc: usize,
    //serde
    endTime: usize,
    //serde
    mediaType: usize,
    //serde
    attributedTo: usize,
    //serde
    generator: usize,
    //serde
    location: usize,
    //serde
    image: usize,
    //serde
    preview: usize,
    //serde
    context: usize,
    //serde
    origin: usize,
    //serde
    object: usize,
    //serde
    attachment: usize,
    //serde
    audience: usize,
    //serde
    tag: usize,
    //serde
    duratio: usize,
    //serde
    actor: usize,
    //serde
    instrument: usize,
    //serde
    target: usize,
    //serde
    updated: usize,
    //serde
    published: usize,
    //serde
    bcc: usize,
    //serde
    startTime: usize,
    //serde
    content: usize,
    //serde
    bto: usize,
    //serde
    url: usize,
    //serde
    summary: usize,
    //serde
    result: usize,
}

#[derive(Debug)]
struct IntransitiveActivity {
    two: String,
    //serde
    attributedTo: usize,
    //serde
    inReplyTo: usize,
    //serde
    published: usize,
    //serde
    icon: usize,
    //serde
    url: usize,
    //serde
    to: usize,
    //serde
    updated: usize,
    //serde
    summary: usize,
    //serde
    cc: usize,
    //serde
    target: usize,
    //serde
    instrument: usize,
    //serde
    attachment: usize,
    //serde
    preview: usize,
    //serde
    image: usize,
    //serde
    object: usize,
    //serde
    audience: usize,
    //serde
    generator: usize,
    //serde
    bcc: usize,
    //serde
    tag: usize,
    //serde
    content: usize,
    //serde
    duratio: usize,
    //serde
    location: usize,
    //serde
    mediaType: usize,
    //serde
    actor: usize,
    //serde
    replies: usize,
    //serde
    result: usize,
    //serde
    endTime: usize,
    //serde
    name: usize,
    //serde
    startTime: usize,
    //serde
    context: usize,
    //serde
    bto: usize,
    //serde
    origin: usize,
}

#[derive(Debug)]
struct Collection {
    two: String,
    //serde
    summary: usize,
    //serde
    content: usize,
    //serde
    mediaType: usize,
    //serde
    bto: usize,
    //serde
    inReplyTo: usize,
    //serde
    last: usize,
    //serde
    cc: usize,
    //serde
    endTime: usize,
    //serde
    duratio: usize,
    //serde
    generator: usize,
    //serde
    attributedTo: usize,
    //serde
    location: usize,
    //serde
    to: usize,
    //serde
    startTime: usize,
    //serde
    current: usize,
    //serde
    replies: usize,
    //serde
    published: usize,
    //serde
    preview: usize,
    //serde
    totalItems: usize,
    //serde
    context: usize,
    //serde
    url: usize,
    //serde
    tag: usize,
    //serde
    first: usize,
    //serde
    updated: usize,
    //serde
    audience: usize,
    //serde
    items: usize,
    //serde
    name: usize,
    //serde
    icon: usize,
    //serde
    bcc: usize,
    //serde
    image: usize,
    //serde
    attachment: usize,
}

#[derive(Debug)]
struct OrderedCollection {
    two: String,
    //serde
    attachment: usize,
    //serde
    totalItems: usize,
    //serde
    summary: usize,
    //serde
    mediaType: usize,
    //serde
    preview: usize,
    //serde
    image: usize,
    //serde
    icon: usize,
    //serde
    url: usize,
    //serde
    cc: usize,
    //serde
    location: usize,
    //serde
    first: usize,
    //serde
    last: usize,
    //serde
    items: usize,
    //serde
    inReplyTo: usize,
    //serde
    bto: usize,
    //serde
    content: usize,
    //serde
    tag: usize,
    //serde
    duratio: usize,
    //serde
    replies: usize,
    //serde
    current: usize,
    //serde
    to: usize,
    //serde
    attributedTo: usize,
    //serde
    startTime: usize,
    //serde
    bcc: usize,
    //serde
    updated: usize,
    //serde
    name: usize,
    //serde
    audience: usize,
    //serde
    endTime: usize,
    //serde
    published: usize,
    //serde
    generator: usize,
    //serde
    context: usize,
}

#[derive(Debug)]
struct CollectionPage {
    two: String,
    //serde
    summary: usize,
    //serde
    tag: usize,
    //serde
    image: usize,
    //serde
    preview: usize,
    //serde
    url: usize,
    //serde
    context: usize,
    //serde
    name: usize,
    //serde
    attributedTo: usize,
    //serde
    first: usize,
    //serde
    location: usize,
    //serde
    generator: usize,
    //serde
    items: usize,
    //serde
    next: usize,
    //serde
    prev: usize,
    //serde
    replies: usize,
    //serde
    endTime: usize,
    //serde
    audience: usize,
    //serde
    published: usize,
    //serde
    partOf: usize,
    //serde
    icon: usize,
    //serde
    last: usize,
    //serde
    content: usize,
    //serde
    inReplyTo: usize,
    //serde
    duratio: usize,
    //serde
    to: usize,
    //serde
    bto: usize,
    //serde
    attachment: usize,
    //serde
    totalItems: usize,
    //serde
    mediaType: usize,
    //serde
    bcc: usize,
    //serde
    startTime: usize,
    //serde
    current: usize,
    //serde
    updated: usize,
    //serde
    cc: usize,
}

#[derive(Debug)]
struct OrderedCollectionPage {
    two: String,
    //serde
    duratio: usize,
    //serde
    name: usize,
    //serde
    updated: usize,
    //serde
    bto: usize,
    //serde
    context: usize,
    //serde
    to: usize,
    //serde
    icon: usize,
    //serde
    first: usize,
    //serde
    cc: usize,
    //serde
    startTime: usize,
    //serde
    totalItems: usize,
    //serde
    startIndex: usize,
    //serde
    url: usize,
    //serde
    audience: usize,
    //serde
    summary: usize,
    //serde
    attributedTo: usize,
    //serde
    bcc: usize,
    //serde
    tag: usize,
    //serde
    replies: usize,
    //serde
    items: usize,
    //serde
    current: usize,
    //serde
    published: usize,
    //serde
    prev: usize,
    //serde
    last: usize,
    //serde
    location: usize,
    //serde
    partOf: usize,
    //serde
    inReplyTo: usize,
    //serde
    next: usize,
    //serde
    generator: usize,
    //serde
    endTime: usize,
    //serde
    image: usize,
    //serde
    mediaType: usize,
    //serde
    preview: usize,
    //serde
    attachment: usize,
    //serde
    content: usize,
}

#[derive(Debug)]
struct Accept {
    two: String,
    //serde
    mediaType: usize,
    //serde
    duratio: usize,
    //serde
    startTime: usize,
    //serde
    endTime: usize,
    //serde
    name: usize,
    //serde
    replies: usize,
    //serde
    audience: usize,
    //serde
    preview: usize,
    //serde
    object: usize,
    //serde
    content: usize,
    //serde
    bcc: usize,
    //serde
    updated: usize,
    //serde
    context: usize,
    //serde
    inReplyTo: usize,
    //serde
    generator: usize,
    //serde
    to: usize,
    //serde
    summary: usize,
    //serde
    origin: usize,
    //serde
    attachment: usize,
    //serde
    url: usize,
    //serde
    tag: usize,
    //serde
    bto: usize,
    //serde
    target: usize,
    //serde
    result: usize,
    //serde
    image: usize,
    //serde
    actor: usize,
    //serde
    instrument: usize,
    //serde
    published: usize,
    //serde
    attributedTo: usize,
    //serde
    location: usize,
    //serde
    cc: usize,
    //serde
    icon: usize,
}

#[derive(Debug)]
struct TentativeAccept {
    two: String,
    //serde
    image: usize,
    //serde
    url: usize,
    //serde
    inReplyTo: usize,
    //serde
    context: usize,
    //serde
    name: usize,
    //serde
    icon: usize,
    //serde
    replies: usize,
    //serde
    bcc: usize,
    //serde
    duratio: usize,
    //serde
    content: usize,
    //serde
    endTime: usize,
    //serde
    instrument: usize,
    //serde
    target: usize,
    //serde
    attachment: usize,
    //serde
    summary: usize,
    //serde
    actor: usize,
    //serde
    startTime: usize,
    //serde
    location: usize,
    //serde
    generator: usize,
    //serde
    cc: usize,
    //serde
    attributedTo: usize,
    //serde
    published: usize,
    //serde
    origin: usize,
    //serde
    mediaType: usize,
    //serde
    tag: usize,
    //serde
    updated: usize,
    //serde
    to: usize,
    //serde
    audience: usize,
    //serde
    bto: usize,
    //serde
    object: usize,
    //serde
    result: usize,
    //serde
    preview: usize,
}

#[derive(Debug)]
struct Add {
    two: String,
    //serde
    result: usize,
    //serde
    bto: usize,
    //serde
    object: usize,
    //serde
    audience: usize,
    //serde
    context: usize,
    //serde
    name: usize,
    //serde
    inReplyTo: usize,
    //serde
    to: usize,
    //serde
    tag: usize,
    //serde
    location: usize,
    //serde
    endTime: usize,
    //serde
    bcc: usize,
    //serde
    attachment: usize,
    //serde
    preview: usize,
    //serde
    mediaType: usize,
    //serde
    generator: usize,
    //serde
    summary: usize,
    //serde
    url: usize,
    //serde
    instrument: usize,
    //serde
    origin: usize,
    //serde
    updated: usize,
    //serde
    published: usize,
    //serde
    icon: usize,
    //serde
    replies: usize,
    //serde
    duratio: usize,
    //serde
    startTime: usize,
    //serde
    attributedTo: usize,
    //serde
    image: usize,
    //serde
    cc: usize,
    //serde
    actor: usize,
    //serde
    target: usize,
    //serde
    content: usize,
}

#[derive(Debug)]
struct Arrive {
    two: String,
    //serde
    duratio: usize,
    //serde
    context: usize,
    //serde
    summary: usize,
    //serde
    result: usize,
    //serde
    audience: usize,
    //serde
    preview: usize,
    //serde
    cc: usize,
    //serde
    icon: usize,
    //serde
    bcc: usize,
    //serde
    attachment: usize,
    //serde
    origin: usize,
    //serde
    mediaType: usize,
    //serde
    endTime: usize,
    //serde
    location: usize,
    //serde
    bto: usize,
    //serde
    url: usize,
    //serde
    startTime: usize,
    //serde
    generator: usize,
    //serde
    inReplyTo: usize,
    //serde
    attributedTo: usize,
    //serde
    tag: usize,
    //serde
    object: usize,
    //serde
    image: usize,
    //serde
    to: usize,
    //serde
    published: usize,
    //serde
    target: usize,
    //serde
    instrument: usize,
    //serde
    updated: usize,
    //serde
    actor: usize,
    //serde
    content: usize,
    //serde
    replies: usize,
    //serde
    name: usize,
}

#[derive(Debug)]
struct Create {
    two: String,
    //serde
    bto: usize,
    //serde
    preview: usize,
    //serde
    audience: usize,
    //serde
    summary: usize,
    //serde
    target: usize,
    //serde
    result: usize,
    //serde
    origin: usize,
    //serde
    updated: usize,
    //serde
    name: usize,
    //serde
    icon: usize,
    //serde
    published: usize,
    //serde
    inReplyTo: usize,
    //serde
    bcc: usize,
    //serde
    to: usize,
    //serde
    image: usize,
    //serde
    attachment: usize,
    //serde
    location: usize,
    //serde
    replies: usize,
    //serde
    content: usize,
    //serde
    duratio: usize,
    //serde
    endTime: usize,
    //serde
    url: usize,
    //serde
    actor: usize,
    //serde
    context: usize,
    //serde
    startTime: usize,
    //serde
    instrument: usize,
    //serde
    cc: usize,
    //serde
    mediaType: usize,
    //serde
    tag: usize,
    //serde
    attributedTo: usize,
    //serde
    generator: usize,
    //serde
    object: usize,
}

#[derive(Debug)]
struct Delete {
    two: String,
    //serde
    result: usize,
    //serde
    attachment: usize,
    //serde
    actor: usize,
    //serde
    image: usize,
    //serde
    summary: usize,
    //serde
    mediaType: usize,
    //serde
    object: usize,
    //serde
    to: usize,
    //serde
    audience: usize,
    //serde
    instrument: usize,
    //serde
    cc: usize,
    //serde
    origin: usize,
    //serde
    context: usize,
    //serde
    name: usize,
    //serde
    replies: usize,
    //serde
    endTime: usize,
    //serde
    tag: usize,
    //serde
    bto: usize,
    //serde
    updated: usize,
    //serde
    inReplyTo: usize,
    //serde
    bcc: usize,
    //serde
    location: usize,
    //serde
    attributedTo: usize,
    //serde
    duratio: usize,
    //serde
    startTime: usize,
    //serde
    target: usize,
    //serde
    content: usize,
    //serde
    generator: usize,
    //serde
    published: usize,
    //serde
    icon: usize,
    //serde
    preview: usize,
    //serde
    url: usize,
}

#[derive(Debug)]
struct Follow {
    two: String,
    //serde
    updated: usize,
    //serde
    tag: usize,
    //serde
    bcc: usize,
    //serde
    replies: usize,
    //serde
    bto: usize,
    //serde
    attributedTo: usize,
    //serde
    icon: usize,
    //serde
    content: usize,
    //serde
    duratio: usize,
    //serde
    attachment: usize,
    //serde
    actor: usize,
    //serde
    result: usize,
    //serde
    preview: usize,
    //serde
    name: usize,
    //serde
    audience: usize,
    //serde
    mediaType: usize,
    //serde
    image: usize,
    //serde
    inReplyTo: usize,
    //serde
    context: usize,
    //serde
    target: usize,
    //serde
    object: usize,
    //serde
    instrument: usize,
    //serde
    url: usize,
    //serde
    published: usize,
    //serde
    location: usize,
    //serde
    generator: usize,
    //serde
    cc: usize,
    //serde
    summary: usize,
    //serde
    startTime: usize,
    //serde
    origin: usize,
    //serde
    to: usize,
    //serde
    endTime: usize,
}

#[derive(Debug)]
struct Ignore {
    two: String,
    //serde
    tag: usize,
    //serde
    image: usize,
    //serde
    endTime: usize,
    //serde
    published: usize,
    //serde
    attachment: usize,
    //serde
    attributedTo: usize,
    //serde
    cc: usize,
    //serde
    result: usize,
    //serde
    bto: usize,
    //serde
    actor: usize,
    //serde
    inReplyTo: usize,
    //serde
    icon: usize,
    //serde
    object: usize,
    //serde
    origin: usize,
    //serde
    context: usize,
    //serde
    updated: usize,
    //serde
    startTime: usize,
    //serde
    preview: usize,
    //serde
    duratio: usize,
    //serde
    target: usize,
    //serde
    audience: usize,
    //serde
    summary: usize,
    //serde
    replies: usize,
    //serde
    bcc: usize,
    //serde
    instrument: usize,
    //serde
    url: usize,
    //serde
    to: usize,
    //serde
    location: usize,
    //serde
    generator: usize,
    //serde
    mediaType: usize,
    //serde
    content: usize,
    //serde
    name: usize,
}

#[derive(Debug)]
struct Join {
    two: String,
    //serde
    target: usize,
    //serde
    location: usize,
    //serde
    to: usize,
    //serde
    summary: usize,
    //serde
    mediaType: usize,
    //serde
    published: usize,
    //serde
    audience: usize,
    //serde
    attachment: usize,
    //serde
    url: usize,
    //serde
    object: usize,
    //serde
    bto: usize,
    //serde
    tag: usize,
    //serde
    duratio: usize,
    //serde
    preview: usize,
    //serde
    inReplyTo: usize,
    //serde
    cc: usize,
    //serde
    instrument: usize,
    //serde
    content: usize,
    //serde
    name: usize,
    //serde
    generator: usize,
    //serde
    startTime: usize,
    //serde
    result: usize,
    //serde
    image: usize,
    //serde
    icon: usize,
    //serde
    bcc: usize,
    //serde
    origin: usize,
    //serde
    endTime: usize,
    //serde
    attributedTo: usize,
    //serde
    updated: usize,
    //serde
    replies: usize,
    //serde
    context: usize,
    //serde
    actor: usize,
}

#[derive(Debug)]
struct Leave {
    two: String,
    //serde
    preview: usize,
    //serde
    url: usize,
    //serde
    origin: usize,
    //serde
    attachment: usize,
    //serde
    attributedTo: usize,
    //serde
    instrument: usize,
    //serde
    inReplyTo: usize,
    //serde
    image: usize,
    //serde
    name: usize,
    //serde
    published: usize,
    //serde
    cc: usize,
    //serde
    target: usize,
    //serde
    content: usize,
    //serde
    result: usize,
    //serde
    actor: usize,
    //serde
    tag: usize,
    //serde
    bcc: usize,
    //serde
    duratio: usize,
    //serde
    object: usize,
    //serde
    updated: usize,
    //serde
    replies: usize,
    //serde
    bto: usize,
    //serde
    generator: usize,
    //serde
    endTime: usize,
    //serde
    icon: usize,
    //serde
    audience: usize,
    //serde
    context: usize,
    //serde
    summary: usize,
    //serde
    to: usize,
    //serde
    startTime: usize,
    //serde
    mediaType: usize,
    //serde
    location: usize,
}

#[derive(Debug)]
struct Like {
    two: String,
    //serde
    origin: usize,
    //serde
    tag: usize,
    //serde
    bcc: usize,
    //serde
    icon: usize,
    //serde
    name: usize,
    //serde
    published: usize,
    //serde
    generator: usize,
    //serde
    location: usize,
    //serde
    result: usize,
    //serde
    updated: usize,
    //serde
    duratio: usize,
    //serde
    summary: usize,
    //serde
    target: usize,
    //serde
    preview: usize,
    //serde
    audience: usize,
    //serde
    attributedTo: usize,
    //serde
    startTime: usize,
    //serde
    actor: usize,
    //serde
    mediaType: usize,
    //serde
    instrument: usize,
    //serde
    object: usize,
    //serde
    endTime: usize,
    //serde
    image: usize,
    //serde
    url: usize,
    //serde
    to: usize,
    //serde
    bto: usize,
    //serde
    content: usize,
    //serde
    cc: usize,
    //serde
    replies: usize,
    //serde
    context: usize,
    //serde
    inReplyTo: usize,
    //serde
    attachment: usize,
}

#[derive(Debug)]
struct Offer {
    two: String,
    //serde
    audience: usize,
    //serde
    endTime: usize,
    //serde
    updated: usize,
    //serde
    image: usize,
    //serde
    name: usize,
    //serde
    bto: usize,
    //serde
    instrument: usize,
    //serde
    startTime: usize,
    //serde
    inReplyTo: usize,
    //serde
    generator: usize,
    //serde
    mediaType: usize,
    //serde
    summary: usize,
    //serde
    to: usize,
    //serde
    object: usize,
    //serde
    preview: usize,
    //serde
    attributedTo: usize,
    //serde
    actor: usize,
    //serde
    icon: usize,
    //serde
    target: usize,
    //serde
    duratio: usize,
    //serde
    content: usize,
    //serde
    attachment: usize,
    //serde
    cc: usize,
    //serde
    replies: usize,
    //serde
    context: usize,
    //serde
    origin: usize,
    //serde
    url: usize,
    //serde
    location: usize,
    //serde
    published: usize,
    //serde
    bcc: usize,
    //serde
    tag: usize,
    //serde
    result: usize,
}

#[derive(Debug)]
struct Invite {
    two: String,
    //serde
    result: usize,
    //serde
    attributedTo: usize,
    //serde
    origin: usize,
    //serde
    target: usize,
    //serde
    endTime: usize,
    //serde
    icon: usize,
    //serde
    content: usize,
    //serde
    tag: usize,
    //serde
    bcc: usize,
    //serde
    to: usize,
    //serde
    object: usize,
    //serde
    startTime: usize,
    //serde
    published: usize,
    //serde
    instrument: usize,
    //serde
    generator: usize,
    //serde
    name: usize,
    //serde
    image: usize,
    //serde
    summary: usize,
    //serde
    cc: usize,
    //serde
    attachment: usize,
    //serde
    mediaType: usize,
    //serde
    actor: usize,
    //serde
    replies: usize,
    //serde
    location: usize,
    //serde
    bto: usize,
    //serde
    context: usize,
    //serde
    preview: usize,
    //serde
    audience: usize,
    //serde
    updated: usize,
    //serde
    duratio: usize,
    //serde
    inReplyTo: usize,
    //serde
    url: usize,
}

#[derive(Debug)]
struct Reject {
    two: String,
    //serde
    instrument: usize,
    //serde
    result: usize,
    //serde
    content: usize,
    //serde
    actor: usize,
    //serde
    bcc: usize,
    //serde
    to: usize,
    //serde
    icon: usize,
    //serde
    bto: usize,
    //serde
    preview: usize,
    //serde
    location: usize,
    //serde
    origin: usize,
    //serde
    name: usize,
    //serde
    updated: usize,
    //serde
    target: usize,
    //serde
    inReplyTo: usize,
    //serde
    startTime: usize,
    //serde
    tag: usize,
    //serde
    duratio: usize,
    //serde
    published: usize,
    //serde
    summary: usize,
    //serde
    attributedTo: usize,
    //serde
    context: usize,
    //serde
    generator: usize,
    //serde
    cc: usize,
    //serde
    attachment: usize,
    //serde
    object: usize,
    //serde
    url: usize,
    //serde
    audience: usize,
    //serde
    endTime: usize,
    //serde
    replies: usize,
    //serde
    mediaType: usize,
    //serde
    image: usize,
}

#[derive(Debug)]
struct TentativeReject {
    two: String,
    //serde
    actor: usize,
    //serde
    result: usize,
    //serde
    bto: usize,
    //serde
    origin: usize,
    //serde
    attachment: usize,
    //serde
    mediaType: usize,
    //serde
    object: usize,
    //serde
    generator: usize,
    //serde
    duratio: usize,
    //serde
    image: usize,
    //serde
    preview: usize,
    //serde
    attributedTo: usize,
    //serde
    location: usize,
    //serde
    endTime: usize,
    //serde
    name: usize,
    //serde
    to: usize,
    //serde
    audience: usize,
    //serde
    published: usize,
    //serde
    target: usize,
    //serde
    content: usize,
    //serde
    startTime: usize,
    //serde
    url: usize,
    //serde
    cc: usize,
    //serde
    context: usize,
    //serde
    summary: usize,
    //serde
    replies: usize,
    //serde
    inReplyTo: usize,
    //serde
    icon: usize,
    //serde
    updated: usize,
    //serde
    tag: usize,
    //serde
    bcc: usize,
    //serde
    instrument: usize,
}

#[derive(Debug)]
struct Remove {
    two: String,
    //serde
    preview: usize,
    //serde
    summary: usize,
    //serde
    endTime: usize,
    //serde
    duratio: usize,
    //serde
    target: usize,
    //serde
    context: usize,
    //serde
    image: usize,
    //serde
    name: usize,
    //serde
    audience: usize,
    //serde
    cc: usize,
    //serde
    origin: usize,
    //serde
    mediaType: usize,
    //serde
    attributedTo: usize,
    //serde
    generator: usize,
    //serde
    bcc: usize,
    //serde
    bto: usize,
    //serde
    startTime: usize,
    //serde
    published: usize,
    //serde
    attachment: usize,
    //serde
    tag: usize,
    //serde
    instrument: usize,
    //serde
    object: usize,
    //serde
    content: usize,
    //serde
    location: usize,
    //serde
    result: usize,
    //serde
    updated: usize,
    //serde
    actor: usize,
    //serde
    icon: usize,
    //serde
    to: usize,
    //serde
    url: usize,
    //serde
    replies: usize,
    //serde
    inReplyTo: usize,
}

#[derive(Debug)]
struct Undo {
    two: String,
    //serde
    generator: usize,
    //serde
    icon: usize,
    //serde
    result: usize,
    //serde
    preview: usize,
    //serde
    origin: usize,
    //serde
    startTime: usize,
    //serde
    cc: usize,
    //serde
    target: usize,
    //serde
    context: usize,
    //serde
    to: usize,
    //serde
    instrument: usize,
    //serde
    summary: usize,
    //serde
    name: usize,
    //serde
    replies: usize,
    //serde
    url: usize,
    //serde
    duratio: usize,
    //serde
    inReplyTo: usize,
    //serde
    actor: usize,
    //serde
    tag: usize,
    //serde
    image: usize,
    //serde
    bcc: usize,
    //serde
    attributedTo: usize,
    //serde
    content: usize,
    //serde
    attachment: usize,
    //serde
    updated: usize,
    //serde
    mediaType: usize,
    //serde
    object: usize,
    //serde
    audience: usize,
    //serde
    location: usize,
    //serde
    bto: usize,
    //serde
    endTime: usize,
    //serde
    published: usize,
}

#[derive(Debug)]
struct Update {
    two: String,
    //serde
    icon: usize,
    //serde
    content: usize,
    //serde
    location: usize,
    //serde
    summary: usize,
    //serde
    mediaType: usize,
    //serde
    attributedTo: usize,
    //serde
    tag: usize,
    //serde
    published: usize,
    //serde
    generator: usize,
    //serde
    inReplyTo: usize,
    //serde
    target: usize,
    //serde
    instrument: usize,
    //serde
    duratio: usize,
    //serde
    actor: usize,
    //serde
    url: usize,
    //serde
    startTime: usize,
    //serde
    attachment: usize,
    //serde
    to: usize,
    //serde
    origin: usize,
    //serde
    cc: usize,
    //serde
    updated: usize,
    //serde
    image: usize,
    //serde
    preview: usize,
    //serde
    bcc: usize,
    //serde
    replies: usize,
    //serde
    name: usize,
    //serde
    object: usize,
    //serde
    context: usize,
    //serde
    audience: usize,
    //serde
    endTime: usize,
    //serde
    bto: usize,
    //serde
    result: usize,
}

#[derive(Debug)]
struct View {
    two: String,
    //serde
    bto: usize,
    //serde
    inReplyTo: usize,
    //serde
    name: usize,
    //serde
    updated: usize,
    //serde
    actor: usize,
    //serde
    summary: usize,
    //serde
    to: usize,
    //serde
    attributedTo: usize,
    //serde
    preview: usize,
    //serde
    generator: usize,
    //serde
    context: usize,
    //serde
    mediaType: usize,
    //serde
    published: usize,
    //serde
    startTime: usize,
    //serde
    instrument: usize,
    //serde
    bcc: usize,
    //serde
    endTime: usize,
    //serde
    content: usize,
    //serde
    target: usize,
    //serde
    image: usize,
    //serde
    object: usize,
    //serde
    duratio: usize,
    //serde
    url: usize,
    //serde
    result: usize,
    //serde
    replies: usize,
    //serde
    cc: usize,
    //serde
    tag: usize,
    //serde
    attachment: usize,
    //serde
    origin: usize,
    //serde
    audience: usize,
    //serde
    icon: usize,
    //serde
    location: usize,
}

#[derive(Debug)]
struct Listen {
    two: String,
    //serde
    origin: usize,
    //serde
    startTime: usize,
    //serde
    location: usize,
    //serde
    attributedTo: usize,
    //serde
    mediaType: usize,
    //serde
    result: usize,
    //serde
    generator: usize,
    //serde
    duratio: usize,
    //serde
    target: usize,
    //serde
    object: usize,
    //serde
    audience: usize,
    //serde
    url: usize,
    //serde
    replies: usize,
    //serde
    name: usize,
    //serde
    icon: usize,
    //serde
    preview: usize,
    //serde
    published: usize,
    //serde
    content: usize,
    //serde
    endTime: usize,
    //serde
    updated: usize,
    //serde
    image: usize,
    //serde
    attachment: usize,
    //serde
    to: usize,
    //serde
    bcc: usize,
    //serde
    summary: usize,
    //serde
    inReplyTo: usize,
    //serde
    context: usize,
    //serde
    bto: usize,
    //serde
    cc: usize,
    //serde
    instrument: usize,
    //serde
    tag: usize,
    //serde
    actor: usize,
}

#[derive(Debug)]
struct Read {
    two: String,
    //serde
    image: usize,
    //serde
    name: usize,
    //serde
    actor: usize,
    //serde
    bto: usize,
    //serde
    published: usize,
    //serde
    mediaType: usize,
    //serde
    context: usize,
    //serde
    preview: usize,
    //serde
    tag: usize,
    //serde
    icon: usize,
    //serde
    target: usize,
    //serde
    replies: usize,
    //serde
    cc: usize,
    //serde
    location: usize,
    //serde
    attributedTo: usize,
    //serde
    instrument: usize,
    //serde
    content: usize,
    //serde
    endTime: usize,
    //serde
    to: usize,
    //serde
    summary: usize,
    //serde
    startTime: usize,
    //serde
    generator: usize,
    //serde
    attachment: usize,
    //serde
    origin: usize,
    //serde
    updated: usize,
    //serde
    bcc: usize,
    //serde
    duratio: usize,
    //serde
    url: usize,
    //serde
    inReplyTo: usize,
    //serde
    result: usize,
    //serde
    object: usize,
    //serde
    audience: usize,
}

#[derive(Debug)]
struct Move {
    two: String,
    //serde
    content: usize,
    //serde
    image: usize,
    //serde
    attributedTo: usize,
    //serde
    attachment: usize,
    //serde
    replies: usize,
    //serde
    actor: usize,
    //serde
    cc: usize,
    //serde
    startTime: usize,
    //serde
    to: usize,
    //serde
    name: usize,
    //serde
    bto: usize,
    //serde
    summary: usize,
    //serde
    inReplyTo: usize,
    //serde
    updated: usize,
    //serde
    origin: usize,
    //serde
    tag: usize,
    //serde
    audience: usize,
    //serde
    duratio: usize,
    //serde
    result: usize,
    //serde
    location: usize,
    //serde
    object: usize,
    //serde
    bcc: usize,
    //serde
    target: usize,
    //serde
    endTime: usize,
    //serde
    published: usize,
    //serde
    instrument: usize,
    //serde
    mediaType: usize,
    //serde
    preview: usize,
    //serde
    icon: usize,
    //serde
    generator: usize,
    //serde
    context: usize,
    //serde
    url: usize,
}

#[derive(Debug)]
struct Travel {
    two: String,
    //serde
    to: usize,
    //serde
    location: usize,
    //serde
    actor: usize,
    //serde
    object: usize,
    //serde
    preview: usize,
    //serde
    replies: usize,
    //serde
    target: usize,
    //serde
    updated: usize,
    //serde
    cc: usize,
    //serde
    endTime: usize,
    //serde
    duratio: usize,
    //serde
    attributedTo: usize,
    //serde
    content: usize,
    //serde
    instrument: usize,
    //serde
    image: usize,
    //serde
    attachment: usize,
    //serde
    summary: usize,
    //serde
    inReplyTo: usize,
    //serde
    bto: usize,
    //serde
    result: usize,
    //serde
    mediaType: usize,
    //serde
    name: usize,
    //serde
    origin: usize,
    //serde
    icon: usize,
    //serde
    context: usize,
    //serde
    tag: usize,
    //serde
    url: usize,
    //serde
    generator: usize,
    //serde
    published: usize,
    //serde
    bcc: usize,
    //serde
    startTime: usize,
    //serde
    audience: usize,
}

#[derive(Debug)]
struct Announce {
    two: String,
    //serde
    target: usize,
    //serde
    attributedTo: usize,
    //serde
    replies: usize,
    //serde
    bcc: usize,
    //serde
    instrument: usize,
    //serde
    actor: usize,
    //serde
    endTime: usize,
    //serde
    startTime: usize,
    //serde
    url: usize,
    //serde
    location: usize,
    //serde
    tag: usize,
    //serde
    attachment: usize,
    //serde
    name: usize,
    //serde
    audience: usize,
    //serde
    result: usize,
    //serde
    bto: usize,
    //serde
    mediaType: usize,
    //serde
    inReplyTo: usize,
    //serde
    duratio: usize,
    //serde
    summary: usize,
    //serde
    context: usize,
    //serde
    object: usize,
    //serde
    content: usize,
    //serde
    updated: usize,
    //serde
    published: usize,
    //serde
    icon: usize,
    //serde
    generator: usize,
    //serde
    cc: usize,
    //serde
    image: usize,
    //serde
    preview: usize,
    //serde
    origin: usize,
    //serde
    to: usize,
}

#[derive(Debug)]
struct Block {
    two: String,
    //serde
    bto: usize,
    //serde
    result: usize,
    //serde
    location: usize,
    //serde
    origin: usize,
    //serde
    audience: usize,
    //serde
    name: usize,
    //serde
    icon: usize,
    //serde
    image: usize,
    //serde
    content: usize,
    //serde
    tag: usize,
    //serde
    published: usize,
    //serde
    object: usize,
    //serde
    duratio: usize,
    //serde
    to: usize,
    //serde
    attributedTo: usize,
    //serde
    inReplyTo: usize,
    //serde
    attachment: usize,
    //serde
    url: usize,
    //serde
    startTime: usize,
    //serde
    mediaType: usize,
    //serde
    replies: usize,
    //serde
    actor: usize,
    //serde
    bcc: usize,
    //serde
    summary: usize,
    //serde
    preview: usize,
    //serde
    instrument: usize,
    //serde
    updated: usize,
    //serde
    endTime: usize,
    //serde
    context: usize,
    //serde
    generator: usize,
    //serde
    cc: usize,
    //serde
    target: usize,
}

#[derive(Debug)]
struct Flag {
    two: String,
    //serde
    to: usize,
    //serde
    result: usize,
    //serde
    generator: usize,
    //serde
    tag: usize,
    //serde
    name: usize,
    //serde
    origin: usize,
    //serde
    audience: usize,
    //serde
    content: usize,
    //serde
    replies: usize,
    //serde
    endTime: usize,
    //serde
    startTime: usize,
    //serde
    attachment: usize,
    //serde
    updated: usize,
    //serde
    published: usize,
    //serde
    bto: usize,
    //serde
    icon: usize,
    //serde
    context: usize,
    //serde
    location: usize,
    //serde
    instrument: usize,
    //serde
    image: usize,
    //serde
    duratio: usize,
    //serde
    preview: usize,
    //serde
    url: usize,
    //serde
    bcc: usize,
    //serde
    attributedTo: usize,
    //serde
    cc: usize,
    //serde
    object: usize,
    //serde
    actor: usize,
    //serde
    target: usize,
    //serde
    inReplyTo: usize,
    //serde
    mediaType: usize,
    //serde
    summary: usize,
}

#[derive(Debug)]
struct Dislike {
    two: String,
    //serde
    generator: usize,
    //serde
    attachment: usize,
    //serde
    tag: usize,
    //serde
    bto: usize,
    //serde
    target: usize,
    //serde
    replies: usize,
    //serde
    cc: usize,
    //serde
    actor: usize,
    //serde
    origin: usize,
    //serde
    published: usize,
    //serde
    to: usize,
    //serde
    endTime: usize,
    //serde
    icon: usize,
    //serde
    inReplyTo: usize,
    //serde
    duratio: usize,
    //serde
    startTime: usize,
    //serde
    name: usize,
    //serde
    audience: usize,
    //serde
    image: usize,
    //serde
    summary: usize,
    //serde
    preview: usize,
    //serde
    result: usize,
    //serde
    bcc: usize,
    //serde
    location: usize,
    //serde
    object: usize,
    //serde
    updated: usize,
    //serde
    instrument: usize,
    //serde
    mediaType: usize,
    //serde
    content: usize,
    //serde
    context: usize,
    //serde
    url: usize,
    //serde
    attributedTo: usize,
}

#[derive(Debug)]
struct Question {
    two: String,
    //serde
    published: usize,
    //serde
    attachment: usize,
    //serde
    audience: usize,
    //serde
    oneOf: usize,
    //serde
    context: usize,
    //serde
    attributedTo: usize,
    //serde
    generator: usize,
    //serde
    closed: usize,
    //serde
    preview: usize,
    //serde
    updated: usize,
    //serde
    origin: usize,
    //serde
    startTime: usize,
    //serde
    actor: usize,
    //serde
    object: usize,
    //serde
    endTime: usize,
    //serde
    bto: usize,
    //serde
    cc: usize,
    //serde
    anyOf: usize,
    //serde
    target: usize,
    //serde
    tag: usize,
    //serde
    content: usize,
    //serde
    mediaType: usize,
    //serde
    replies: usize,
    //serde
    duratio: usize,
    //serde
    location: usize,
    //serde
    result: usize,
    //serde
    to: usize,
    //serde
    summary: usize,
    //serde
    inReplyTo: usize,
    //serde
    image: usize,
    //serde
    url: usize,
    //serde
    instrument: usize,
    //serde
    bcc: usize,
    //serde
    name: usize,
    //serde
    icon: usize,
}

#[derive(Debug)]
struct Application {
    two: String,
    //serde
    summary: usize,
    //serde
    audience: usize,
    //serde
    bto: usize,
    //serde
    published: usize,
    //serde
    icon: usize,
    //serde
    url: usize,
    //serde
    context: usize,
    //serde
    tag: usize,
    //serde
    mediaType: usize,
    //serde
    content: usize,
    //serde
    generator: usize,
    //serde
    startTime: usize,
    //serde
    endTime: usize,
    //serde
    updated: usize,
    //serde
    name: usize,
    //serde
    cc: usize,
    //serde
    duratio: usize,
    //serde
    attributedTo: usize,
    //serde
    location: usize,
    //serde
    preview: usize,
    //serde
    bcc: usize,
    //serde
    inReplyTo: usize,
    //serde
    image: usize,
    //serde
    to: usize,
    //serde
    replies: usize,
    //serde
    attachment: usize,
}

#[derive(Debug)]
struct Group {
    two: String,
    //serde
    name: usize,
    //serde
    url: usize,
    //serde
    location: usize,
    //serde
    inReplyTo: usize,
    //serde
    image: usize,
    //serde
    audience: usize,
    //serde
    startTime: usize,
    //serde
    attachment: usize,
    //serde
    preview: usize,
    //serde
    updated: usize,
    //serde
    replies: usize,
    //serde
    context: usize,
    //serde
    icon: usize,
    //serde
    summary: usize,
    //serde
    published: usize,
    //serde
    duratio: usize,
    //serde
    endTime: usize,
    //serde
    generator: usize,
    //serde
    to: usize,
    //serde
    tag: usize,
    //serde
    cc: usize,
    //serde
    mediaType: usize,
    //serde
    attributedTo: usize,
    //serde
    bcc: usize,
    //serde
    bto: usize,
    //serde
    content: usize,
}

#[derive(Debug)]
struct Organization {
    two: String,
    //serde
    audience: usize,
    //serde
    image: usize,
    //serde
    content: usize,
    //serde
    location: usize,
    //serde
    attributedTo: usize,
    //serde
    updated: usize,
    //serde
    url: usize,
    //serde
    icon: usize,
    //serde
    bcc: usize,
    //serde
    name: usize,
    //serde
    cc: usize,
    //serde
    preview: usize,
    //serde
    to: usize,
    //serde
    endTime: usize,
    //serde
    bto: usize,
    //serde
    startTime: usize,
    //serde
    attachment: usize,
    //serde
    inReplyTo: usize,
    //serde
    generator: usize,
    //serde
    published: usize,
    //serde
    tag: usize,
    //serde
    duratio: usize,
    //serde
    mediaType: usize,
    //serde
    summary: usize,
    //serde
    context: usize,
    //serde
    replies: usize,
}

#[derive(Debug)]
struct Person {
    two: String,
    //serde
    image: usize,
    //serde
    replies: usize,
    //serde
    duratio: usize,
    //serde
    bto: usize,
    //serde
    attributedTo: usize,
    //serde
    bcc: usize,
    //serde
    audience: usize,
    //serde
    endTime: usize,
    //serde
    updated: usize,
    //serde
    generator: usize,
    //serde
    attachment: usize,
    //serde
    cc: usize,
    //serde
    to: usize,
    //serde
    startTime: usize,
    //serde
    content: usize,
    //serde
    mediaType: usize,
    //serde
    summary: usize,
    //serde
    location: usize,
    //serde
    preview: usize,
    //serde
    context: usize,
    //serde
    inReplyTo: usize,
    //serde
    published: usize,
    //serde
    icon: usize,
    //serde
    name: usize,
    //serde
    tag: usize,
    //serde
    url: usize,
}

#[derive(Debug)]
struct Service {
    two: String,
    //serde
    attributedTo: usize,
    //serde
    generator: usize,
    //serde
    location: usize,
    //serde
    audience: usize,
    //serde
    tag: usize,
    //serde
    context: usize,
    //serde
    icon: usize,
    //serde
    name: usize,
    //serde
    to: usize,
    //serde
    updated: usize,
    //serde
    content: usize,
    //serde
    replies: usize,
    //serde
    summary: usize,
    //serde
    endTime: usize,
    //serde
    preview: usize,
    //serde
    attachment: usize,
    //serde
    startTime: usize,
    //serde
    inReplyTo: usize,
    //serde
    bto: usize,
    //serde
    url: usize,
    //serde
    cc: usize,
    //serde
    published: usize,
    //serde
    image: usize,
    //serde
    duratio: usize,
    //serde
    mediaType: usize,
    //serde
    bcc: usize,
}

#[derive(Debug)]
struct Relationship {
    two: String,
    //serde
    updated: usize,
    //serde
    attachment: usize,
    //serde
    bcc: usize,
    //serde
    inReplyTo: usize,
    //serde
    audience: usize,
    //serde
    location: usize,
    //serde
    subject: usize,
    //serde
    duratio: usize,
    //serde
    cc: usize,
    //serde
    context: usize,
    //serde
    url: usize,
    //serde
    published: usize,
    //serde
    image: usize,
    //serde
    attributedTo: usize,
    //serde
    endTime: usize,
    //serde
    bto: usize,
    //serde
    content: usize,
    //serde
    name: usize,
    //serde
    mediaType: usize,
    //serde
    startTime: usize,
    //serde
    replies: usize,
    //serde
    summary: usize,
    //serde
    relationship: usize,
    //serde
    to: usize,
    //serde
    tag: usize,
    //serde
    generator: usize,
    //serde
    object: usize,
    //serde
    preview: usize,
    //serde
    icon: usize,
}

#[derive(Debug)]
struct Article {
    two: String,
    //serde
    attachment: usize,
    //serde
    icon: usize,
    //serde
    endTime: usize,
    //serde
    duratio: usize,
    //serde
    published: usize,
    //serde
    summary: usize,
    //serde
    attributedTo: usize,
    //serde
    content: usize,
    //serde
    name: usize,
    //serde
    bto: usize,
    //serde
    audience: usize,
    //serde
    to: usize,
    //serde
    generator: usize,
    //serde
    cc: usize,
    //serde
    bcc: usize,
    //serde
    tag: usize,
    //serde
    replies: usize,
    //serde
    updated: usize,
    //serde
    mediaType: usize,
    //serde
    startTime: usize,
    //serde
    inReplyTo: usize,
    //serde
    location: usize,
    //serde
    preview: usize,
    //serde
    image: usize,
    //serde
    url: usize,
    //serde
    context: usize,
}

#[derive(Debug)]
struct Document {
    two: String,
    //serde
    endTime: usize,
    //serde
    tag: usize,
    //serde
    context: usize,
    //serde
    bto: usize,
    //serde
    attributedTo: usize,
    //serde
    to: usize,
    //serde
    audience: usize,
    //serde
    url: usize,
    //serde
    preview: usize,
    //serde
    cc: usize,
    //serde
    updated: usize,
    //serde
    replies: usize,
    //serde
    attachment: usize,
    //serde
    location: usize,
    //serde
    mediaType: usize,
    //serde
    published: usize,
    //serde
    duratio: usize,
    //serde
    icon: usize,
    //serde
    image: usize,
    //serde
    name: usize,
    //serde
    bcc: usize,
    //serde
    generator: usize,
    //serde
    startTime: usize,
    //serde
    summary: usize,
    //serde
    inReplyTo: usize,
    //serde
    content: usize,
}

#[derive(Debug)]
struct Audio {
    two: String,
    //serde
    location: usize,
    //serde
    image: usize,
    //serde
    context: usize,
    //serde
    attachment: usize,
    //serde
    audience: usize,
    //serde
    summary: usize,
    //serde
    icon: usize,
    //serde
    bto: usize,
    //serde
    name: usize,
    //serde
    attributedTo: usize,
    //serde
    endTime: usize,
    //serde
    content: usize,
    //serde
    tag: usize,
    //serde
    duratio: usize,
    //serde
    to: usize,
    //serde
    mediaType: usize,
    //serde
    replies: usize,
    //serde
    published: usize,
    //serde
    generator: usize,
    //serde
    cc: usize,
    //serde
    updated: usize,
    //serde
    url: usize,
    //serde
    startTime: usize,
    //serde
    bcc: usize,
    //serde
    preview: usize,
    //serde
    inReplyTo: usize,
}

#[derive(Debug)]
struct Image {
    two: String,
    //serde
    startTime: usize,
    //serde
    inReplyTo: usize,
    //serde
    attachment: usize,
    //serde
    summary: usize,
    //serde
    name: usize,
    //serde
    url: usize,
    //serde
    content: usize,
    //serde
    context: usize,
    //serde
    mediaType: usize,
    //serde
    endTime: usize,
    //serde
    attributedTo: usize,
    //serde
    published: usize,
    //serde
    to: usize,
    //serde
    image: usize,
    //serde
    duratio: usize,
    //serde
    tag: usize,
    //serde
    bto: usize,
    //serde
    generator: usize,
    //serde
    cc: usize,
    //serde
    location: usize,
    //serde
    preview: usize,
    //serde
    audience: usize,
    //serde
    replies: usize,
    //serde
    bcc: usize,
    //serde
    updated: usize,
    //serde
    icon: usize,
}

#[derive(Debug)]
struct Video {
    two: String,
    //serde
    inReplyTo: usize,
    //serde
    url: usize,
    //serde
    summary: usize,
    //serde
    icon: usize,
    //serde
    startTime: usize,
    //serde
    tag: usize,
    //serde
    content: usize,
    //serde
    mediaType: usize,
    //serde
    bto: usize,
    //serde
    preview: usize,
    //serde
    bcc: usize,
    //serde
    name: usize,
    //serde
    attributedTo: usize,
    //serde
    cc: usize,
    //serde
    published: usize,
    //serde
    to: usize,
    //serde
    replies: usize,
    //serde
    duratio: usize,
    //serde
    attachment: usize,
    //serde
    updated: usize,
    //serde
    endTime: usize,
    //serde
    location: usize,
    //serde
    generator: usize,
    //serde
    audience: usize,
    //serde
    context: usize,
    //serde
    image: usize,
}

#[derive(Debug)]
struct Note {
    two: String,
    //serde
    startTime: usize,
    //serde
    updated: usize,
    //serde
    bto: usize,
    //serde
    icon: usize,
    //serde
    audience: usize,
    //serde
    bcc: usize,
    //serde
    mediaType: usize,
    //serde
    tag: usize,
    //serde
    preview: usize,
    //serde
    image: usize,
    //serde
    url: usize,
    //serde
    to: usize,
    //serde
    location: usize,
    //serde
    published: usize,
    //serde
    name: usize,
    //serde
    cc: usize,
    //serde
    content: usize,
    //serde
    replies: usize,
    //serde
    attachment: usize,
    //serde
    generator: usize,
    //serde
    endTime: usize,
    //serde
    inReplyTo: usize,
    //serde
    duratio: usize,
    //serde
    summary: usize,
    //serde
    context: usize,
    //serde
    attributedTo: usize,
}

#[derive(Debug)]
struct Page {
    two: String,
    //serde
    duratio: usize,
    //serde
    generator: usize,
    //serde
    endTime: usize,
    //serde
    startTime: usize,
    //serde
    image: usize,
    //serde
    to: usize,
    //serde
    location: usize,
    //serde
    preview: usize,
    //serde
    content: usize,
    //serde
    bcc: usize,
    //serde
    replies: usize,
    //serde
    context: usize,
    //serde
    bto: usize,
    //serde
    inReplyTo: usize,
    //serde
    published: usize,
    //serde
    mediaType: usize,
    //serde
    tag: usize,
    //serde
    attributedTo: usize,
    //serde
    url: usize,
    //serde
    updated: usize,
    //serde
    icon: usize,
    //serde
    attachment: usize,
    //serde
    cc: usize,
    //serde
    name: usize,
    //serde
    summary: usize,
    //serde
    audience: usize,
}

#[derive(Debug)]
struct Event {
    two: String,
    //serde
    replies: usize,
    //serde
    bcc: usize,
    //serde
    url: usize,
    //serde
    content: usize,
    //serde
    image: usize,
    //serde
    audience: usize,
    //serde
    published: usize,
    //serde
    attributedTo: usize,
    //serde
    inReplyTo: usize,
    //serde
    summary: usize,
    //serde
    name: usize,
    //serde
    cc: usize,
    //serde
    preview: usize,
    //serde
    attachment: usize,
    //serde
    bto: usize,
    //serde
    startTime: usize,
    //serde
    location: usize,
    //serde
    tag: usize,
    //serde
    context: usize,
    //serde
    to: usize,
    //serde
    updated: usize,
    //serde
    duratio: usize,
    //serde
    generator: usize,
    //serde
    mediaType: usize,
    //serde
    endTime: usize,
    //serde
    icon: usize,
}

#[derive(Debug)]
struct Place {
    two: String,
    //serde
    audience: usize,
    //serde
    preview: usize,
    //serde
    attributedTo: usize,
    //serde
    units: usize,
    //serde
    inReplyTo: usize,
    //serde
    context: usize,
    //serde
    accuracy: usize,
    //serde
    attachment: usize,
    //serde
    icon: usize,
    //serde
    startTime: usize,
    //serde
    location: usize,
    //serde
    longitude: usize,
    //serde
    content: usize,
    //serde
    name: usize,
    //serde
    published: usize,
    //serde
    cc: usize,
    //serde
    bto: usize,
    //serde
    mediaType: usize,
    //serde
    updated: usize,
    //serde
    altitude: usize,
    //serde
    latitude: usize,
    //serde
    url: usize,
    //serde
    to: usize,
    //serde
    summary: usize,
    //serde
    radius: usize,
    //serde
    endTime: usize,
    //serde
    tag: usize,
    //serde
    bcc: usize,
    //serde
    image: usize,
    //serde
    generator: usize,
    //serde
    duratio: usize,
    //serde
    replies: usize,
}

#[derive(Debug)]
struct Mention {
    two: String,
    //serde
    rel: usize,
    //serde
    height: usize,
    //serde
    width: usize,
    //serde
    name: usize,
    //serde
    mediaType: usize,
    //serde
    href: usize,
    //serde
    previe: usize,
    //serde
    hreflang: usize,
}

#[derive(Debug)]
struct Profile {
    two: String,
    //serde
    duratio: usize,
    //serde
    published: usize,
    //serde
    cc: usize,
    //serde
    name: usize,
    //serde
    icon: usize,
    //serde
    attachment: usize,
    //serde
    endTime: usize,
    //serde
    describes: usize,
    //serde
    bcc: usize,
    //serde
    context: usize,
    //serde
    startTime: usize,
    //serde
    updated: usize,
    //serde
    url: usize,
    //serde
    content: usize,
    //serde
    generator: usize,
    //serde
    audience: usize,
    //serde
    image: usize,
    //serde
    summary: usize,
    //serde
    attributedTo: usize,
    //serde
    tag: usize,
    //serde
    inReplyTo: usize,
    //serde
    preview: usize,
    //serde
    bto: usize,
    //serde
    replies: usize,
    //serde
    mediaType: usize,
    //serde
    location: usize,
    //serde
    to: usize,
}

#[derive(Debug)]
struct Tombstone {
    two: String,
    //serde
    attributedTo: usize,
    //serde
    updated: usize,
    //serde
    audience: usize,
    //serde
    image: usize,
    //serde
    bcc: usize,
    //serde
    tag: usize,
    //serde
    context: usize,
    //serde
    published: usize,
    //serde
    mediaType: usize,
    //serde
    replies: usize,
    //serde
    url: usize,
    //serde
    summary: usize,
    //serde
    icon: usize,
    //serde
    preview: usize,
    //serde
    location: usize,
    //serde
    to: usize,
    //serde
    cc: usize,
    //serde
    attachment: usize,
    //serde
    deleted: usize,
    //serde
    generator: usize,
    //serde
    endTime: usize,
    //serde
    startTime: usize,
    //serde
    inReplyTo: usize,
    //serde
    duratio: usize,
    //serde
    name: usize,
    //serde
    content: usize,
    //serde
    bto: usize,
    //serde
    formerType: usize,
}